{ system, self, nixpkgs, inputs, user, ... }:

let
  pkgs = import nixpkgs {
    inherit system;
    config.allowUnfree = true;
  };

  lib = nixpkgs.lib;

in
{

  users.users.${user} = {
  uid = 1000;
  isNormalUser = true;
  home = "/home/${user}";
  description = "${user}";
  extraGroups = [ "wheel" "networkmanager" ];
  };

  nixosConfigurations = {

    fenris = lib.nixosSystem {
      specialArgs = { inherit inputs user system; };
      modules = [
        ./fenris
        ../modules/system.nix
        ../modules/grub.nix
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            nixosModules.home-manager
            extraSpecialArgs = { inherit user; };
            users.${user} = {
                imports = [
                  ./home/home.nix
                ];
            };
          };
        }
      ];
    };

    garm = lib.nixosSystem {
      specialArgs = { inherit inputs user system; };
      modules = [
        ./garm
        ../modules/system.nix
        ../modules/grub.nix
        {
          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            nixosModules.home-manager
            extraSpecialArgs = { inherit user; };
            users.${user} = {
                imports = [
                  ./home/home.nix
                ];
            };
          };
        }
      ];
    };

  };
}